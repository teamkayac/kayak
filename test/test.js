var system = require('../server/api/db').collection('system')
var ObjectId = require('../server/node_modules/mongolian').ObjectId
var items = ["SPECIFIQUE", "CARDIO", "RENFORCEMENT MUSCULAIRE", "SOINS", "STRATEGIE PROJET"]

var procede = ["Endurance",
    "SV 1",
    "SV 2",
    "PMA / VMA",
    "Vitesse de course",
    "Vitesse",
    "Technique",
    "RM F Endurance",
    "RM Force Puissance",
    "RM Force Max",
    " RM Force vitesse",
];


var types = {
    "slalom_sans_portes": "Slalom sans portes",
    "slalom_avec_portes": "Slalom avec portes",
    "bateau_directeur": "Bateau directeur",
    "specifique_autre": "Autre (Spécifique)",
    "course": "Course à pieds",
    "velo": "Velo de route",
    "vtt": "VTT",
    "ski_de_fond": "Ski de fond",
    "natation": "Natation",
    "surf": "Surf",
    "sport_raquette": "Sport de raquette",
    "cardio_autre": "Autre (Cardio)",
    "rm_salle": "Renforcement Musculaire en salle",
    "rm_embarquee": "Renforcement musculaire embarqué",
    "competition": "Compétitions & Tests",
    "slalom": "Slalom",
    "test_terrain": "Test terrain",
    "test_hrv": "Test HRV",
    "rm_autre": "Autre (Renforcement musculaire)",
    "kine_osteo": "Kiné/Ostéo",
    "balneo": "Balnéothérapie",
    "relaxation_yoga": "Relaxation/Yoga",
    "chryotherapie": "Chryotherapie",
    "etirements": "Etirements",
    "soins_autre": "Autre (Soins)",
    "analyse_video": "Analyse vidéo",
    "entretien": "Entretien avec entraîneur",
    "gestion_conception": "Gestion, conception, entretien matos",
    "travail_projet_vie": "Travail sur le projet de vie",
    "logistique": "Logistique saison, course, actions",
    "strategie_autre": "Autre (Stratégie)"
};
Array.prototype.randItem = function () {
    return this[Math.floor(Math.random() * this.length)];
}

var i = 0;
var a = []
var i = 4
var nb = Math.ceil(Math.random() * 100)
nb = 0


var weight = []
var weightFact = 0.08

var height = []
var heightFact = 0.03

var fat_per = []

var hr_standing = []
var hrv = []

var toDay = 144;

for (var j = 1; j <= 365; j++) {
    var is_filled = Math.random() > 0.2
    var poids = Math.random() * 100;
    var date = new Date(2016, 0, j);
    var _id = nb++
    var toPush = {
        "_id": new ObjectId(),
        date: date,
        type: Object.keys(types).randItem(),
        "procede": procede.randItem(),
        filled: is_filled,
        globaltype: items.randItem(),
        heure_deb: 1,
        heure_fin: 1 + Math.ceil(Math.random() * 10),
        distance: Math.random() * 10,
        ressenti: is_filled ? [1, 2, 3, 3, 3, 4, 4, 4, 4, 5].randItem() : 0,
        "feedback": {
            "eval_objective": Math.random()*5,
            "eval_sensations": Math.random()*5,
            "eval_fatigue": Math.random()*5,
            "objectives_text": "",
            "temps_travail_intensite": "",
            "difficulte": "",
            "nb_portes_franchies": "",
            "nb_erreurs": "",
            "nb_parcours_realises": "",
            "nb_parcours_a_zero": "",
            "nb_penalites_seance": "",
            "nb_km": "",
            "fc_moyenne": "",
            "fc_max": ""
        },
        titre : "Training "+Math.random()
    };
    a.push(toPush)

    weight.push({date: date, value: j * weightFact + Math.random() * 2 + 85})
    height.push({date: date, value: j * heightFact + 175})
    fat_per.push({date: date, value: Math.random() * 5 + 10})
    hr_standing.push({date: date, value: Math.ceil(Math.random() * 5 + 70)})
    hrv.push({date: date, value: Math.ceil(Math.random() * 5 + 2)})

}

var profiles = ["Grégoire"]

for (var i in profiles) {

    var profile = profiles[i]
    console.log(profile)
    system.update({
        "profile.name": profile
    }, {
        "$set": {
            "trainings": a
        }
    }, function (err, arr) {
        console.log(nb)
    })
    system.update({
        "profile.name": profile
    }, {
        "$set": {
            "stats.weight": weight
        }
    }, function (err, arr) {
        console.log(nb)
    })
    system.update({
        "profile.name": profile
    }, {
        "$set": {
            "stats.height": height
        }
    }, function (err, arr) {
        console.log(nb)
    })
    system.update({
        "profile.name": profile
    }, {
        "$set": {
            "stats.fat_percentage": fat_per
        }
    }, function (err, arr) {
        console.log(nb)
    })
    system.update({
        "profile.name": profile
    }, {
        "$set": {
            "stats.hr_standing": hr_standing
        }
    }, function (err, arr) {
        console.log(nb)
    })
    system.update({
        "profile.name": profile
    }, {
        "$set": {
            "stats.hrv_test": hrv
        }
    }, function (err, arr) {
        console.log(nb)
    })
}